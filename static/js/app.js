$(document).ready(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            $('.navbar').css('background-color', '#fcbad3');
        } else {
            $('.navbar').css('background-color', '#aa96da');
        }
    });
});